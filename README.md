<div align="center">
  <img src="https://framagit.org/Grokafar/gitlab-monitor-rs/-/raw/master/static/img/gitlab-logo.svg" width="150" />

  <h1>Gitlab Monitor</h1>

  <p>
    <strong>Rust written dashboard for Gitlab monitoring</strong>
  </p>

  <p>
    <a href="https://blog.rust-lang.org/2021/09/09/Rust-1.55.0.html">
      <img alt="Rustc Version 1.55+" src="https://img.shields.io/badge/rustc-1.55+-lightgrey.svg"/>
    </a>
  </p>

  <h4>
    <a href="https://framagit.org/Grokafar/gitlab-monitor-rs/-/issues">Issues</a>
  </h4>
</div>

## About

Gitlab Monitor is a dashboard written in Rust (with [yew](https://github.com/yewstack/yew)), for monitoring Gitlab CI builds.

![UI](https://framagit.org/Grokafar/gitlab-monitor-rs/-/raw/master/static/img/screenshots/ui.jpg)

## Development

* Gitlab API : <https://docs.rs/gitlab/>
* Yew : <https://docs.rs/yew/>

### Setup

```bash
cargo install wasm-pack
```

### Build dev

```bash
wasm-pack build --target web -d static/pkg --dev
python server.py 42000
# Open http://localhost:42000
```

### Build release

```bash
wasm-pack build --target web -d static/pkg
python server.py 42000
# Open http://localhost:42000
```

## Credits

* <https://gitlab.com/gitlab-org/gitlab-svgs>
* <https://github.com/globocom/gitlab-ci-monitor>
* <https://github.com/KSF-Media/gitlab-dashboard>
* [Not tested] <https://github.com/timoschwarzer/gitlab-monitor>
* [Not tested] <https://www.npmjs.com/package/gitlab-ci-dashboard>
