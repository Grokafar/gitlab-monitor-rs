(function(){

    var TagsInput = function(opts){
        if (typeof opts == "string") {
            var options = {
                selector: opts
            }
        } else {
            var options = opts
        }
        this.options = Object.assign(TagsInput.defaults , options);
        this.input = document.getElementById(options.selector);
        console.log("Binding TagsInput on "+options.selector)
        this.arr = [];
        this.wrapper = document.createElement('div');
        buildUI(this);
        addEvents(this);
    }

    TagsInput.prototype.addTag = function(string){

        if(this.anyErrors(string))
            return ;

        this.arr.push(string);
        var tagInput = this;


        var tag = document.createElement('span');
        tag.className = this.options.tagClass;
        tag.innerText = string;

        var closeIcon = document.createElement('a');
        closeIcon.innerHTML = '&times;';
        closeIcon.addEventListener('click' , function(e){
            e.preventDefault();
            var tag = this.parentNode;

            for(var i =0 ;i < tagInput.wrapper.childNodes.length ; i++){
                if(tagInput.wrapper.childNodes[i] == tag)
                    tagInput.deleteTag(tag , i);
            }
        })


        tag.appendChild(closeIcon);
        this.wrapper.insertBefore(tag , this.input);
        // this.input.value = this.arr.join(',');

        return this;
    }



    TagsInput.prototype.deleteTag = function(tag , i){
        tag.remove();
        this.arr.splice( i , 1);
        // this.input.value =  this.arr.join(',');
        return this;
    }


    TagsInput.prototype.anyErrors = function (string) {
        if (this.options.max != null && this.arr.length >= this.options.max) {
            console.error('max tags limit reached');
            return true;
        }
    
        if (!this.options.duplicate && this.arr.indexOf(string) != -1) {
            console.error('duplicate found " ' + string + ' " ')
            return true;
        }
    
        if (this.options.validator != undefined && !this.options.validator(string)) {
            console.error('Invalid input: ' + string)
            return true;
        }
    
        return false;
    }


    TagsInput.prototype.addData = function(array){
        var plugin = this;
        
        array.forEach(function(string){
            plugin.addTag(string);
        })
        return this;
    }

    
    TagsInput.prototype.getInputString = function(){
        console.log("Getting values for " + this.input.id)
        return this.arr.join(',');
    }


    // Private function to initialize the UI Elements
    function buildUI(tags){
        tags.wrapper.classList.add(tags.options.wrapperClass);
        tags.input.parentNode.insertBefore(tags.wrapper , null);
        tags.wrapper.append(tags.input);
    }

    function custom_trim(string, c) {
      var re = new RegExp("^[" + c + "]+|[" + c + "]+$", "g");
      return string.replace(re,"");
    }

    function addEvents(tags){
        tags.wrapper.addEventListener('click' ,function(){
            tags.input.focus();           
        });
        tags.input.addEventListener('keydown' , function(e){
            var str = custom_trim(tags.input.value, ", ");
            if( !!(~["Tab" , "Enter" , ",", ";"   ].indexOf( e.key ))  )
            {
                tags.input.value = "";
                if(str != "")
                    tags.addTag(str);

                e.preventDefault();
                e.stopPropagation();
            }
        });
    }


    TagsInput.defaults = {
        selector : '',
        wrapperClass : 'tags-input-wrapper',
        tagClass : 'tag',
        max : null,
        duplicate: false
    }


    window.TagsInput = TagsInput;

})();

export function init_input_tag(input_id) { return new TagsInput(input_id); }