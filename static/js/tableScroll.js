$(document).ready(function() {
    pageScroll();
    $("#tableScroll").mouseover(function() {
        clearTimeout(my_time);
    }).mouseout(function() {
        pageScroll();
    });
});

var my_time;

function pageScroll() {
    var objDiv = document.getElementById("tableScroll");
    var delay = 100;

    // If the end is not reached
    if ((objDiv.scrollTop + objDiv.offsetHeight) < objDiv.scrollHeight) {
        objDiv.scrollTop = objDiv.scrollTop + 2;
        // If the end is now reached, sleep
        if ((objDiv.scrollTop + objDiv.offsetHeight) >= objDiv.scrollHeight) {
            delay = 1000;
        }
        // If the end is reached, back to the top, and sleep
    } else {
        objDiv.scrollTop = 0;
        delay = 1000;
    }

    my_time = setTimeout('pageScroll()', delay);
}