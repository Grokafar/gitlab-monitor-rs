import init, { run_app } from './pkg/gitlab_monitor.js';
async function main() {
   await init('pkg/gitlab_monitor_bg.wasm');
   run_app();
}
main()
