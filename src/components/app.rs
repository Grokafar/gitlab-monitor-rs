use crate::{
    components::{config_form::ConfigForm, dashboard::Dashboard},
    config::AppConfig,
    GITLAB_SERVICE,
};
use log::*;
use wasm_bindgen::prelude::*;
use yew::{prelude::*, Component, ComponentLink, Html, ShouldRender};
use yew_router::{service::RouteService, Switch};

pub struct App {
    configured: bool,
}

pub enum AppMsg {}

// Order is important. First matching route will be used.
#[derive(Switch, Debug)]
pub enum AppRoute {
    #[to = "/{_prefix}#config={config}"]
    Run { _prefix: String, config: AppConfig },
    #[to = "/"]
    Config,
}

impl Component for App {
    type Message = AppMsg;
    type Properties = ();

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        true
    }

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        trace!("Create App component");
        let route_service: RouteService<()> = RouteService::new();

        // Try to get AppConfig from URL
        let config = if let Some(AppRoute::Run { _prefix, config }) =
            AppRoute::switch(route_service.get_route())
        {
            debug!("Valid config found in URL.");
            Some(config)
        } else {
            debug!("No config found in URL.");
            None
        };
        let app = App {
            configured: config.is_some(),
        };
        // If config is OK the update the App
        if let Some(config) = config {
            GITLAB_SERVICE.write().unwrap_throw().update_config(config);
        };

        app
    }
    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }
    fn view(&self) -> Html {
        let gitlab_service_r = GITLAB_SERVICE.read().unwrap_throw();
        let config = gitlab_service_r.get_config();
        html! {
            <div>
                {
                    if !self.configured {
                        html!{
                            <style>
                                {"body {background: #dedede;}"}
                            </style>
                        }
                    } else if config.dark_mode {
                        html!{
                            <style>
                                {"body {background: #3C454D;}"}
                            </style>
                        }
                    } else {
                        html!{}
                    }
                }
                <nav class="navbar navbar-dark bg-dark mb-4">
                    <a class="navbar-brand">
                        <img src="img/gitlab-logo.svg" width="21" height="21" class="d-inline-block align-top" alt="Gitlab logo"/>
                        <span>{"Gitlab Monitor"}</span>
                    </a>
                    <div class="btn-group">
                        // TODO: Restore the button somewhere
                        // <button class="btn btn-primary" onclick=self.link.callback(|_| Msg::UpdateProjectList)>
                        //     <img src="img/sprite_icons_repeat.svg" alt="Update all"/>
                        // </button>
                        {
                            if self.configured {
                                html!{
                                    <button class="btn btn-secondary" data-toggle="modal" data-target="#settingsModal">
                                        <img src="img/sprite_icons_settings.svg" alt="Settings"/>
                                    </button>
                                }
                            } else {
                                html!{}
                            }
                        }
                    </div>
                </nav>

                <div class="container-fluid">
                    {
                        if !self.configured {
                            html!{
                                <div class="modal fade show" id="settingsModal" style="display: block;">
                                    <ConfigForm config={config.clone()} />
                                </div>
                            }
                        } else {
                            html!{
                                <div>
                                    <div class="modal fade" id="settingsModal">
                                        <ConfigForm config={config.clone()} />
                                    </div>
                                    <Dashboard/>
                                </div>
                            }
                        }
                    }
                </div>
            </div>
        }
    }
}
