use crate::{gitlab_service::LastPipeline, GITLAB_SERVICE};
use gitlab::{ProjectId, StatusState};
use log::error;
use wasm_bindgen::prelude::*;
use yew::prelude::*;

pub struct ProjectCard {
    pub project_id: ProjectId,
    pub per_line: u64,
    pub hide_no_ci: bool,
}

#[derive(Properties, Clone)]
pub struct ProjectProps {
    pub project_id: ProjectId,
    pub per_line: u64,
    pub hide_no_ci: bool,
}

pub enum Msg {
    Error,
    Nope,
}

impl Component for ProjectCard {
    type Message = Msg;
    type Properties = ProjectProps;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        ProjectCard {
            project_id: props.project_id,
            per_line: props.per_line,
            hide_no_ci: props.hide_no_ci,
        }
    }
    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.project_id = props.project_id;
        true
    }
    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Error => {
                error!("Error");
                false
            }
            Msg::Nope => false,
        }
    }
    fn view(&self) -> Html {
        let gitlab_service_r = GITLAB_SERVICE.read().unwrap_throw();
        match gitlab_service_r.get_project(&self.project_id) {
            None => html! {},
            Some(project) => {
                let bg = match gitlab_service_r.get_last_pipeline(&self.project_id) {
                    LastPipeline::NotAvailable
                    | LastPipeline::Unknown
                    | LastPipeline::NetworkError => "bg-secondary".to_string(),
                    LastPipeline::Pipeline(last_pipeline) => match last_pipeline.status {
                        StatusState::Created | StatusState::Pending | StatusState::Running => {
                            "bg-primary".to_string()
                        }
                        StatusState::Success => "bg-success".to_string(),
                        StatusState::Failed => "bg-danger".to_string(),
                        StatusState::Canceled => "bg-dark".to_string(),
                        StatusState::Skipped | StatusState::Manual => "bg-warning".to_string(),
                        StatusState::WaitingForResource => "bg-warning".to_string(),
                        StatusState::Preparing => "bg-warning".to_string(),
                        StatusState::Scheduled => "bg-warning".to_string(),
                    },
                };
                let hide = match gitlab_service_r.get_last_pipeline(&self.project_id) {
                    LastPipeline::NotAvailable
                    | LastPipeline::Unknown
                    | LastPipeline::NetworkError => self.hide_no_ci,
                    LastPipeline::Pipeline(_) => false,
                };
                let card_style;
                let card_responsive;
                if self.per_line > 0 {
                    card_style = format!(
                        "min-width: calc((100% - {}px) / {}) !important;",
                        (30 * self.per_line),
                        self.per_line
                    );
                    card_responsive = String::from("");
                } else {
                    card_style = String::from("");
                    card_responsive = String::from("card-responsive");
                }
                if !hide {
                    html! {
                        <div class={format!("card {} text-white mb-3 {}",bg, card_responsive)} style={card_style}>
                            <div class="card-body">
                                <h5 class="card-title">
                                    <a href={project.web_url.clone()} target="_blank">
                                        {&project.name}
                                    </a>
                                    {
                                        match &gitlab_service_r.get_last_pipeline(&self.project_id)  {
                                            LastPipeline::Pipeline(last_pipeline) => {
                                                match &last_pipeline.ref_ {
                                                    Some(ref_)=>{
                                                        if last_pipeline.tag {
                                                            html!{<span class="badge badge-dark">{ref_}</span>}
                                                        }
                                                        else {
                                                            html!{}
                                                        }
                                                    },
                                                    None=>html!{}
                                                }
                                            }
                                            _ => html!{}
                                        }
                                    }
                                </h5>
                                <p class="card-text text-truncate">
                                {
                                    match &gitlab_service_r.get_repo_commit_status().get(&self.project_id) {
                                        Some(details) => details.message.clone(),
                                        None => "No commit".to_string()
                                    }
                                }
                                </p>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-auto">
                                        <a href=format!("{}/pipelines", project.web_url.as_str()) target="_blank">
                                            <img src="img/sprite_icons_ci.svg" alt="Coverage"/>
                                            {
                                                match &gitlab_service_r.get_last_pipeline(&self.project_id)  {
                                                    LastPipeline::Pipeline(last_pipeline) => {
                                                        if let Some(created_at) = last_pipeline.created_at {
                                                            let formatter = timeago::Formatter::new();
                                                            html!{
                                                                <span data-toggle="tooltip" title={created_at.with_timezone::<chrono::FixedOffset>(&chrono::TimeZone::from_offset(chrono::Local::now().offset())).format("Last pipeline: %F %T").to_string()}>
                                                                    {format!(" {}", formatter.convert((chrono::offset::Utc::now() - created_at).to_std().unwrap_throw()))}
                                                                </span>
                                                            }

                                                        }
                                                        else {
                                                            html!{}
                                                        }
                                                    }
                                                    _ =>{
                                                        html!{<>{" No CI"}</>}
                                                    }
                                                }
                                            }
                                        </a>
                                    </div>
                                    <div class="col-auto ml-auto">
                                        <a href=format!("{}/commits", project.web_url.as_str()) target="_blank">
                                            <img src="img/sprite_icons_commit.svg" alt="Last commit date"/>
                                            {
                                                match &gitlab_service_r.get_repo_commit_status().get(&self.project_id) {
                                                    Some(details) => {
                                                        let formatter = timeago::Formatter::new();
                                                        html!{
                                                            <span data-toggle="tooltip" title={details.committed_date.with_timezone::<chrono::FixedOffset>(&chrono::TimeZone::from_offset(chrono::Local::now().offset())).format("Last commit: %F %T").to_string()}>
                                                                {format!(" {}", formatter.convert((chrono::offset::Utc::now() - details.committed_date).to_std().unwrap_throw()))}
                                                            </span>
                                                        }
                                                    },
                                                    None => html!{{" No commit"}}
                                                }
                                            }
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-auto">
                                        <a href=format!("{}/issues", project.web_url.as_str()) target="_blank">
                                            <img src="img/sprite_icons_issues.svg" alt="Open issues"/>
                                            {format!(" {} issues", project.open_issues_count.unwrap_or(0))}
                                        </a>
                                    </div>
                                    {
                                        match &gitlab_service_r.get_last_pipeline(&self.project_id)  {
                                            LastPipeline::Pipeline(last_pipeline) => {
                                                if let Some(coverage) = &last_pipeline.coverage {
                                                    html!{
                                                        <div class="col-auto ml-auto mr-auto">
                                                            <a href=format!("{}/pipelines", project.web_url.as_str()) target="_blank">
                                                                <img src="img/sprite_icons_push-rules.svg" alt="Coverage"/>
                                                                {format!(" {}%",coverage)}
                                                            </a>
                                                        </div>
                                                    }
                                                }
                                                else {
                                                    html!{}
                                                }
                                            }
                                            _ =>{
                                                html!{}
                                            }
                                        }
                                    }
                                    <div class="col-auto ml-auto">
                                        <a href=format!("{}/merge_requests", project.web_url.as_str()) target="_blank">
                                            <img src="img/sprite_icons_merge-request.svg" alt="Open merges request"/>
                                            {format!(" {} MR", gitlab_service_r.get_merge_request_count(&self.project_id).unwrap_or(&0))}
                                        </a>
                                    </div>
                                </div>
                            </div>
                            // TODO Move that shit away!
                            <script>{"$(function () {$('[data-toggle=\"tooltip\"]').tooltip()})"}</script>

                        </div>
                    }
                } else {
                    html! {}
                }
            }
        }
    }
}
