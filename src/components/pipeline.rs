use crate::{components::pipeline_timer::PipelineTimerComponent, GITLAB_SERVICE};
use gitlab::{Job, PipelineBasic, ProjectId, StatusState};
use std::collections::{BTreeMap, HashMap};
use wasm_bindgen::UnwrapThrowExt;
use yew::prelude::*;

pub struct PipelineComponent {
    project_id: ProjectId,
    pipeline_basic: PipelineBasic,
    not_running_and_jobs_fetched: bool,
}

#[derive(Properties, Clone)]
pub struct PipelineProps {
    pub project_id: ProjectId,
    pub pipeline_basic: PipelineBasic,
}

pub enum Msg {
    QueryJobs,
}

impl Component for PipelineComponent {
    type Message = Msg;
    type Properties = PipelineProps;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        let mut pipeline = PipelineComponent {
            project_id: props.project_id,
            pipeline_basic: props.pipeline_basic,
            not_running_and_jobs_fetched: false,
        };
        pipeline.update(Self::Message::QueryJobs);
        pipeline
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::QueryJobs => true,
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.pipeline_basic.id != props.pipeline_basic.id
            || self.pipeline_basic.status != props.pipeline_basic.status
            || self.pipeline_basic.updated_at != props.pipeline_basic.updated_at
            || props.pipeline_basic.status == StatusState::Pending
            || props.pipeline_basic.status == StatusState::Created
            || props.pipeline_basic.status == StatusState::Running
        {
            self.project_id = props.project_id;
            self.pipeline_basic = props.pipeline_basic;
            self.not_running_and_jobs_fetched = false;
            true
        } else {
            let gitlab_service_r = GITLAB_SERVICE.read().unwrap_throw();
            match gitlab_service_r.get_jobs(&self.pipeline_basic.id) {
                Some(jobs) => {
                    if jobs
                        .iter()
                        .filter_map(|job| {
                            if job.status == StatusState::Pending
                                || job.status == StatusState::Created
                                || job.status == StatusState::Running
                            {
                                Some(job)
                            } else {
                                None
                            }
                        })
                        .next()
                        .is_some()
                    {
                        return true;
                    }

                    if self.not_running_and_jobs_fetched {
                        false
                    } else {
                        self.not_running_and_jobs_fetched = true;
                        true
                    }
                }
                None => false,
            }
        }
    }

    fn view(&self) -> Html {
        let gitlab_service_r = GITLAB_SERVICE.read().unwrap_throw();

        let (color, status_text) = match self.pipeline_basic.status {
            StatusState::Created => ("bg-warning".to_string(), "Created"),
            StatusState::Pending => ("bg-warning".to_string(), "Pending"),
            StatusState::Running => ("bg-primary".to_string(), "Running"),
            StatusState::Success => ("bg-success".to_string(), "Success"),
            StatusState::Failed => ("bg-danger".to_string(), "Failed"),
            StatusState::Canceled => ("bg-dark".to_string(), "Canceled"),
            StatusState::Skipped => ("bg-warning".to_string(), "Skipped"),
            StatusState::Manual => ("bg-warning".to_string(), "Manual"),
            StatusState::WaitingForResource => ("bg-warning".to_string(), "WaitingForResource"),
            StatusState::Preparing => ("bg-warning".to_string(), "Preparing"),
            StatusState::Scheduled => ("bg-warning".to_string(), "Scheduled"),
        };

        let (project_name, project_url) = match gitlab_service_r.get_project(&self.project_id) {
            Some(project) => (project.name_with_namespace.clone(), project.web_url.clone()),
            None => (self.project_id.to_string(), String::new()),
        };

        if let Some(jobs) = gitlab_service_r.get_jobs(&self.pipeline_basic.id) {
            let mut stages: HashMap<String, BTreeMap<String, &Job>> = HashMap::new();
            let mut stages_order: Vec<String> = Vec::new();
            for job in jobs {
                match stages.get_mut(&job.stage) {
                    Some(list) => match list.get(&job.name) {
                        Some(value) => {
                            if value.id < job.id {
                                list.insert(job.name.clone(), job);
                            }
                        }
                        None => {
                            list.insert(job.name.clone(), job);
                        }
                    },
                    None => {
                        let mut list = BTreeMap::new();
                        list.insert(job.name.clone(), job);
                        stages.insert(job.stage.clone(), list);
                        stages_order.push(job.stage.clone());
                    }
                }
            }

            let mut first = true;
            html! {
                <tr class={color}>
                    <td>
                        <div><b>{status_text}</b></div>
                        <div>
                            <a href={self.pipeline_basic.web_url.clone()} target="_blank">{"#"}{self.pipeline_basic.id}</a>
                        </div>
                    </td>
                    <td>
                        <div><b><a href={project_url} target="_blank">{project_name}</a></b></div>
                        <div>
                            <img src="img/sprite_icons_branch.svg" alt="Branch"/> {format!(" {} ", &self.pipeline_basic.ref_.as_ref().unwrap_or(&"".to_string()))}
                            {
                                if !jobs.is_empty() {
                                    html!{
                                        <span>
                                            <img src="img/sprite_icons_commit.svg" alt="Commit"/>
                                            { format!(" {}", &jobs[0].commit.title) }
                                        </span>
                                    }
                                } else {
                                    html!{}
                                }
                            }
                        </div>
                    </td>
                    {
                        if let Some(date_created) = self.pipeline_basic.created_at {
                            html!{
                                <td>
                                    <div>
                                        <img src="img/sprite_icons_calendar.svg" alt="Date"/>
                                        {
                                            html!{
                                                <span data-toggle="tooltip" title={date_created.with_timezone::<chrono::FixedOffset>(&chrono::TimeZone::from_offset(chrono::Local::now().offset())).format("Creation date: %F %T").to_string()}>
                                                    {
                                                        if self.pipeline_basic.status == StatusState::Pending
                                                            || self.pipeline_basic.status == StatusState::Created
                                                            || self.pipeline_basic.status == StatusState::Running {
                                                            html!{
                                                                <PipelineTimerComponent timeago=true interval=1 pipeline_status=self.pipeline_basic.status start_date=date_created end_date=None />
                                                            }
                                                        } else {
                                                            html!{
                                                                <PipelineTimerComponent timeago=true interval=30 pipeline_status=self.pipeline_basic.status start_date=date_created end_date=None />
                                                            }
                                                        }
                                                    }
                                                </span>
                                            }
                                        }
                                    </div>
                                    {
                                        if let Some(date_updated) = self.pipeline_basic.updated_at {
                                            html!{
                                                <div>
                                                    <img src="img/sprite_icons_timer.svg" alt="Duration"/>
                                                    <span data-toggle="tooltip" title="Pipeline duration">
                                                        {
                                                            if self.pipeline_basic.status == StatusState::Pending
                                                                || self.pipeline_basic.status == StatusState::Created
                                                                || self.pipeline_basic.status == StatusState::Running {
                                                                html!{
                                                                    <PipelineTimerComponent timeago=false interval=1 pipeline_status=self.pipeline_basic.status start_date=date_created end_date=None />
                                                                }
                                                            } else {
                                                                html!{
                                                                    <PipelineTimerComponent timeago=false interval=30 pipeline_status=self.pipeline_basic.status start_date=date_created end_date=Some(date_updated) />
                                                                }
                                                            }
                                                        }
                                                    </span>
                                                </div>
                                            }
                                        } else {
                                            html!{}
                                        }
                                    }
                                </td>
                            }
                        } else {
                            html!{<td></td>}
                        }
                    }
                    <td style="vertical-align: middle;">
                        <div>
                        {
                            stages_order.iter().map( |stage| {
                                let jobs = stages.get(stage).unwrap();
                                let more_than_one_job = jobs.len() > 1;
                                let stage = stage.clone();

                                html!{
                                    <group>
                                    {
                                        if first {
                                            first = false;
                                            html!{}
                                        } else {
                                            html!{<img class="img-fluid" src="img/sprite_icons_angle-right.svg" />}
                                        }
                                    }
                                    {
                                        if more_than_one_job && jobs.values().all(|job| job.status == StatusState::Created) {
                                            html!{<a href={self.pipeline_basic.web_url.clone()} target="_blank"><img class="img-fluid" style="width: 26px" src="img/sprite_icons_status_created_multiple.svg" data-toggle="tooltip" title={stage} /> </a>}
                                        } else if more_than_one_job && jobs.values().all(|job| job.status == StatusState::Pending) {
                                            html!{<a href={self.pipeline_basic.web_url.clone()} target="_blank"><img class="img-fluid" style="width: 26px" src="img/sprite_icons_status_pending_multiple.svg" data-toggle="tooltip" title={stage} /> </a>}
                                        } else if more_than_one_job && jobs.values().all(|job| job.status == StatusState::Running) {
                                            html!{<a href={self.pipeline_basic.web_url.clone()} target="_blank"><img class="img-fluid" style="width: 26px" src="img/sprite_icons_status_running_multiple.svg" data-toggle="tooltip" title={stage} /> </a>}
                                        } else if more_than_one_job && jobs.values().all(|job| job.status == StatusState::Success) {
                                            html!{<a href={self.pipeline_basic.web_url.clone()} target="_blank"><img class="img-fluid" style="width: 26px" src="img/sprite_icons_status_success_multiple.svg" data-toggle="tooltip" title={stage} /> </a>}
                                        } else if more_than_one_job && jobs.values().all(|job| job.status == StatusState::Success || (job.allow_failure && job.status == StatusState::Failed)) {
                                            html!{<a href={self.pipeline_basic.web_url.clone()} target="_blank"><img class="img-fluid" style="width: 26px" src="img/sprite_icons_status_warning_multiple.svg" data-toggle="tooltip" title={stage} /> </a>}
                                        } else if more_than_one_job && jobs.values().all(|job| job.status == StatusState::Failed) {
                                            html!{<a href={self.pipeline_basic.web_url.clone()} target="_blank"><img class="img-fluid" style="width: 26px" src="img/sprite_icons_status_failed_multiple.svg" data-toggle="tooltip" title={stage} /> </a>}
                                        } else if more_than_one_job && jobs.values().all(|job| job.status == StatusState::Canceled) {
                                            html!{<a href={self.pipeline_basic.web_url.clone()} target="_blank"><img class="img-fluid" style="width: 26px" src="img/sprite_icons_status_canceled_multiple.svg" data-toggle="tooltip" title={stage} /> </a>}
                                        } else if more_than_one_job && jobs.values().all(|job| job.status == StatusState::Skipped) {
                                            html!{<a href={self.pipeline_basic.web_url.clone()} target="_blank"><img class="img-fluid" style="width: 26px" src="img/sprite_icons_status_skipped_multiple.svg" data-toggle="tooltip" title={stage} /> </a>}
                                        } else if more_than_one_job && jobs.values().all(|job| job.status == StatusState::Manual) {
                                            html!{<a href={self.pipeline_basic.web_url.clone()} target="_blank"><img class="img-fluid" style="width: 26px" src="img/sprite_icons_status_manual_multiple.svg" data-toggle="tooltip" title={stage} /> </a>}
                                        } else {
                                            jobs.values().map(|job|
                                                html!{
                                                    <a href={job.web_url.clone()} target="_blank">
                                                    {
                                                        match job.status {
                                                            StatusState::Created =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_created.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                            StatusState::Pending =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_pending.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                            StatusState::Running =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_running.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                            StatusState::Success =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_success.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                            StatusState::Failed if job.allow_failure =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_warning.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                            StatusState::Failed =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_failed.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                            StatusState::Canceled =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_canceled.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                            StatusState::Skipped =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_skipped.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                            StatusState::Manual =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_manual.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                            StatusState::WaitingForResource =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_pending.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                            StatusState::Preparing =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_pending.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                            StatusState::Scheduled =>html!{<img class="img-fluid" style="width: 20px" src="img/sprite_icons_status_pending.svg" data-toggle="tooltip" title={format!{"{}.{}", stage,job.name}} />},
                                                        }

                                                    }
                                                    </a>
                                                }
                                            ).collect::<Html>()
                                        }
                                    }
                                    </group>
                                }
                            }).collect::<Html>()
                        }
                        </div>
                    </td>
                    // TODO Move that shit away!
                    <script>{"$(function () {$('[data-toggle=\"tooltip\"]').tooltip()})"}</script>

                </tr>
            }
        } else {
            html! {}
        }
    }
}
