use crate::{
    callback,
    components::{pipeline::PipelineComponent, project_card::ProjectCard},
    gitlab_service::{LastPipeline, PER_PAGE},
    GITLAB_SERVICE,
};
use anyhow::Error;
use chrono::Utc;
use gitlab::{
    types::Project, Job, MergeRequest, Pipeline, PipelineBasic, PipelineId, ProjectId,
    RepoCommitDetail, StatusState,
};
use log::*;
use std::{
    collections::{hash_map::Entry, HashMap},
    time::Duration,
};
use wasm_bindgen::prelude::*;
use yew::{
    format::Json,
    prelude::*,
    services::{
        fetch::{FetchTask, Response, StatusCode},
        interval::IntervalTask,
        IntervalService,
    },
};

pub struct Dashboard {
    link: ComponentLink<Self>,
    groups_ft: HashMap<String, FetchTask>,
    projects_list_ft: HashMap<String, FetchTask>,
    projects_ft: HashMap<ProjectId, FetchTask>,
    details_ft: HashMap<ProjectId, FetchTask>,
    merge_request_count_ft: HashMap<ProjectId, FetchTask>,
    pipelines_ft: HashMap<ProjectId, FetchTask>,
    jobs_ft: HashMap<PipelineId, FetchTask>,
    last_pipelines_ft: HashMap<ProjectId, FetchTask>,
    public_projects_ft: Option<FetchTask>,
    _update_list_it: IntervalTask,
    update_details_it: HashMap<ProjectId, IntervalTask>,
    update_jobs_it: HashMap<PipelineId, IntervalTask>,
}

pub enum Msg {
    UpdateProjectList,
    UpdateProjectInfos(ProjectId),
    Error(String),
    AddPipelines((ProjectId, Vec<PipelineBasic>)),
    AddGroupProjects {
        namespace: String,
        projects: Vec<Project>,
        page: usize,
    },
    AddPublicProjects {
        projects: Vec<Project>,
        page: usize,
    },
    AddProject(Box<(String, Project)>),
    UpdateProjectDone(ProjectId, Option<String>),
    UpdateDetailsDone(ProjectId),
    UpdateMergeRequestCounterDone(ProjectId),
    UpdateJobsDone(PipelineId),
    UpdateLastPipelineDone(ProjectId),
    UpdateJobs(ProjectId, PipelineId),
}

impl Component for Dashboard {
    type Message = Msg;
    type Properties = ();

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        true
    }

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        trace!("Create App component");

        // Interval Task updating the project list from the config
        let update_list_callback = link.callback(|_| Self::Message::UpdateProjectList);
        let update_list_it = IntervalService::spawn(
            Duration::from_secs(
                GITLAB_SERVICE
                    .read()
                    .unwrap()
                    .get_config()
                    .update_projects_list_interval_secs,
            ),
            update_list_callback,
        );

        let mut dashboard = Dashboard {
            link,
            details_ft: HashMap::new(),
            last_pipelines_ft: HashMap::new(),
            groups_ft: HashMap::new(),
            projects_list_ft: HashMap::new(),
            projects_ft: HashMap::new(),
            merge_request_count_ft: HashMap::new(),
            pipelines_ft: HashMap::new(),
            jobs_ft: HashMap::new(),
            public_projects_ft: None,
            _update_list_it: update_list_it,
            update_details_it: HashMap::new(),
            update_jobs_it: HashMap::new(),
        };
        dashboard.update(Self::Message::UpdateProjectList);

        dashboard
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            // Get all jobs for the iven pipeline
            Self::Message::UpdateJobs(project_id, pipeline_id) => {
                let callback =
                    self.link
                        .callback(move |response: Response<Json<Result<Vec<Job>, Error>>>| {
                            let (meta, Json(res)) = response.into_parts();
                            if meta.status == StatusCode::OK {
                                match res {
                                    Ok(jobs) => {
                                        let mut gitlab_service_w =
                                            GITLAB_SERVICE.write().unwrap_throw();
                                        gitlab_service_w.update_jobs(&pipeline_id, jobs);
                                        Msg::UpdateJobsDone(pipeline_id)
                                    }
                                    Err(e) => Msg::Error(format!(
                                        "Cannot parse jobs from {} because {:?}",
                                        &pipeline_id, e
                                    )),
                                }
                            } else {
                                Msg::Error(format!(
                                    "Cannot fetch jobs form {} because {}",
                                    pipeline_id, meta.status
                                ))
                            }
                        });
                self.jobs_ft.insert(
                    pipeline_id,
                    GITLAB_SERVICE
                        .read()
                        .unwrap_throw()
                        .fetch_jobs_from_pipeline_id(project_id, &pipeline_id, callback)
                        .unwrap_throw(),
                );
                true
            }
            // Get all project info (Project, RepeDetails, LastPipeline)
            Msg::UpdateProjectInfos(project_id) => {
                debug!("Update Project Infos");
                match GITLAB_SERVICE
                    .read()
                    .unwrap_throw()
                    .get_project(&project_id)
                {
                    // Project still exist in the service
                    Some(_project) => {
                        self.update_project(project_id);
                        self.update_pipelines(project_id);
                    }
                    // The project has been drop by the service
                    None => {
                        self.update_details_it.remove(&project_id);
                    }
                };

                true
            }
            // Get projects from the given config
            Msg::UpdateProjectList => {
                debug!("Update Project List");
                // Mutex on removing old pipelines
                {
                    GITLAB_SERVICE.write().unwrap_throw().remove_old_pipelines();
                }
                let gitlab_service_r = GITLAB_SERVICE.read().unwrap_throw();
                let config = gitlab_service_r.get_config();
                // Projects from `groups` field
                for group in &config.groups {
                    debug!("Group {}", group);
                    self.groups_ft.insert(
                        group.clone(),
                        gitlab_service_r
                            .fetch_projects_by_group(
                                group,
                                self.add_group_projects_callback(group, 1),
                                1,
                            )
                            .unwrap_throw(),
                    );
                }

                // Projects from `projects` field
                for project in &config.projects {
                    debug!("Project {}", project);
                    let project_clone = project.clone();
                    let callback = self.link.callback(
                        move |response: Response<Json<Result<Project, Error>>>| {
                            let (meta, Json(res)) = response.into_parts();
                            if meta.status == StatusCode::OK {
                                match res {
                                    Ok(project) => Msg::AddProject(Box::new((
                                        project_clone.to_string(),
                                        project,
                                    ))),
                                    Err(e) => Msg::Error(format!(
                                        "Cannot add project : {} ({:?})",
                                        project_clone, e
                                    )),
                                }
                            } else {
                                Msg::Error(format!(
                                    "Cannot fetch project {} (status {})",
                                    project_clone, meta.status
                                ))
                            }
                        },
                    );
                    self.projects_list_ft.insert(
                        project.clone(),
                        gitlab_service_r
                            .fetch_project_from_alias(project, callback)
                            .unwrap_throw(),
                    );
                }

                // Public projects if any
                if config.public_projects {
                    debug!("Fetching Public Projects");
                    let page = 1;
                    self.public_projects_ft = Some(
                        gitlab_service_r
                            .fetch_public_projects(self.add_public_projects_callback(page), page)
                            .unwrap_throw(),
                    );
                }
                false
            }
            // Add project to the App from a same group.
            // `namespace`, is only need for remove the associated FetchingTask.
            // `project.namespace` is used for storage
            Msg::AddGroupProjects {
                namespace,
                projects,
                page,
            } => {
                // Not the last page, fetching the next one
                if projects.len() == PER_PAGE {
                    let gitlab_service_r = GITLAB_SERVICE.read().unwrap_throw();
                    let page = page + 1;
                    debug!("Fetch next projects page {}", page);

                    self.groups_ft.insert(
                        namespace.clone(),
                        gitlab_service_r
                            .fetch_projects_by_group(
                                &namespace,
                                self.add_group_projects_callback(&namespace, page),
                                page,
                            )
                            .unwrap_throw(),
                    );
                } else {
                    self.groups_ft.remove(&namespace);
                }
                for project in projects {
                    self.add_project(project);
                }

                true
            }
            Msg::AddPublicProjects { projects, page } => {
                if projects.len() == PER_PAGE {
                    let page = page + 1;
                    debug!("Next public projects page:{}", page);
                    self.public_projects_ft = Some(
                        GITLAB_SERVICE
                            .read()
                            .unwrap_throw()
                            .fetch_public_projects(self.add_public_projects_callback(page), page)
                            .unwrap_throw(),
                    );
                } else {
                    self.public_projects_ft = None;
                }
                for project in projects {
                    self.add_project(project);
                }

                true
            }
            Msg::AddProject(box_tuple) => {
                self.add_project(box_tuple.1);
                self.projects_list_ft.remove(&box_tuple.0);
                true
            }
            Msg::AddPipelines((project_id, pipelines)) => {
                let base_it_sec = GITLAB_SERVICE
                    .read()
                    .unwrap()
                    .get_config()
                    .update_jobs_interval_secs;

                for pipeline in pipelines {
                    let pipeline_id = pipeline.id;
                    let pipeline_status = pipeline.status;
                    let updated_at = pipeline
                        .updated_at
                        .or_else(|| Some(Utc::now()))
                        .expect("Can't be None here.");
                    let elapsed_since_updated_at = Utc::now().timestamp() - updated_at.timestamp();
                    let mut update_jobs = false;
                    update_jobs = update_jobs
                        || GITLAB_SERVICE
                            .write()
                            .unwrap_throw()
                            .update_pipeline(project_id, pipeline)
                            .is_none();

                    // Fetch pipeline jobs only if the pipeline is not Success // TODO: why
                    update_jobs = update_jobs
                        || if pipeline_status == StatusState::Success
                            && elapsed_since_updated_at > base_it_sec as i64 * 2
                        {
                            // Update jobs for the last time
                            self.update_jobs_it.remove(&pipeline_id).is_some()
                        } else if let Entry::Vacant(e) = self.update_jobs_it.entry(pipeline_id) {
                            let update_jobs_callback = self.link.callback(move |_| {
                                Self::Message::UpdateJobs(project_id, pipeline_id)
                            });
                            let it_seed = (rand::random::<f64>() * 1000f64).round() as u64;

                            e.insert(IntervalService::spawn(
                                Duration::from_millis(base_it_sec * 1000 + it_seed),
                                update_jobs_callback,
                            ));
                            // New pipeline => update jobs
                            true
                        } else {
                            // Pipeline is known and the interval_task is still running
                            false
                        };

                    if update_jobs {
                        self.update(Self::Message::UpdateJobs(project_id, pipeline_id));
                    }
                }
                self.pipelines_ft.remove(&project_id);
                true
            }
            Msg::Error(e) => {
                error!("Error: {}", e);
                false
            }
            // UpdateDetailsDone: now fetching other data which needs `details` infos
            Msg::UpdateDetailsDone(project_id) => {
                self.details_ft.remove(&project_id);
                let mut gitlab_service_w = GITLAB_SERVICE.write().unwrap_throw();
                if let Some(details) = gitlab_service_w.get_repo_details(&project_id) {
                    // Fetch LastPipeline if needed
                    match &details.last_pipeline {
                        Some(last_pipeline) => {
                            let callback = self.link.callback(
                                move |response: Response<Json<Result<Pipeline, Error>>>| {
                                    let mut gitlab_service_w =
                                        GITLAB_SERVICE.write().unwrap_throw();

                                    let (meta, Json(res)) = response.into_parts();
                                    let updated_state = if meta.status == StatusCode::OK {
                                        match res {
                                            Ok(pipeline) => {
                                                LastPipeline::Pipeline(Box::new(pipeline))
                                            }
                                            Err(e) => {
                                                error!("Cannot parse pipeline {:?}", e);
                                                LastPipeline::NotAvailable
                                            }
                                        }
                                    } else if meta.status == StatusCode::NOT_FOUND
                                        || meta.status == StatusCode::UNAUTHORIZED
                                        || meta.status == StatusCode::FORBIDDEN
                                        || meta.status == StatusCode::BAD_REQUEST
                                        || meta.status == StatusCode::METHOD_NOT_ALLOWED
                                    {
                                        warn!("Pipeline not available (code:{})", meta.status);
                                        LastPipeline::NotAvailable
                                    } else {
                                        warn!("Pipeline network error (code:{})", meta.status);
                                        LastPipeline::NetworkError
                                    };
                                    gitlab_service_w
                                        .update_last_pipeline(project_id, updated_state);
                                    Msg::UpdateLastPipelineDone(project_id)
                                },
                            );
                            self.last_pipelines_ft.insert(
                                project_id,
                                gitlab_service_w
                                    .fetch_pipeline_from_pipeline_id(
                                        project_id,
                                        &last_pipeline.id,
                                        callback,
                                    )
                                    .unwrap_throw(),
                            );
                        }
                        None => {
                            gitlab_service_w
                                .update_last_pipeline(project_id, LastPipeline::NotAvailable);
                        }
                    }
                }
                true
            }
            Msg::UpdateProjectDone(project_id, default_branch) => {
                self.projects_ft.remove(&project_id);
                self.update_details(project_id, default_branch);
                true
            }
            Msg::UpdateMergeRequestCounterDone(project_id) => {
                self.merge_request_count_ft.remove(&project_id);
                true
            }
            Msg::UpdateLastPipelineDone(project_id) => {
                self.last_pipelines_ft.remove(&project_id);
                true
            }
            Msg::UpdateJobsDone(pipeline_id) => {
                self.jobs_ft.remove(&pipeline_id);
                true
            }
        }
    }

    fn view(&self) -> Html {
        let gitlab_service_r = GITLAB_SERVICE.read().unwrap_throw();
        let config = gitlab_service_r.get_config();
        html! {
            <div>
                <div class="row mb-4">
                    <div class="col-12">
                    {
                        gitlab_service_r.get_projects_group_by_group()
                        .iter()
                        .filter(|(_namespace, projects)| {
                            projects
                                .iter()
                                .any(|project| match gitlab_service_r.get_last_pipeline(project.1) {
                                    LastPipeline::Pipeline(_) => true,
                                    _ => !gitlab_service_r.get_config().hide_no_ci,
                                })
                        })
                        .map( |(namespace, projects)| {
                            html!{
                                <div class=format!("card bg-{} text-{} mb-2",
                                    match config.dark_mode {
                                        true => "secondary",
                                        false => "light",
                                    },
                                    match config.dark_mode {
                                        true => "light",
                                        false => "dark",
                                    })>
                                    <h5 class="card-header">{namespace}</h5>
                                    <div class="card-body pb-1">
                                        <div class="card-deck">
                                            {
                                                // TODO: Sort before render
                                                projects.iter().map(|(_name,project_id)| {
                                                    html!{
                                                        <ProjectCard project_id={*project_id} per_line={config.per_line} hide_no_ci={config.hide_no_ci}/> }
                                                }).collect::<Html>()
                                            }
                                        </div>
                                    </div>
                                </div>
                            }
                        })
                        .collect::<Html>()
                    }
                    </div>
                </div>

                <div class="row mb-4">
                    <div class="col-12">
                    <div class=format!("card bg-{} text-{}",
                            match config.dark_mode {
                                true => "secondary",
                                false => "light",
                            },
                            match config.dark_mode {
                                true => "white",
                                false => "dark",
                            })>
                            <h5 class="card-header">{"Latest Pipelines"}</h5>
                            <div class="card-body pb-1">
                                <table class="table table-dark text-white">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">{"Pipeline"}</th>
                                            <th scope="col">{"Commit"}</th>
                                            <th scope="col">{"Time"}</th>
                                            <th scope="col">{"Stage"}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        gitlab_service_r.get_pipelines().iter().rev().map( |(_, tuple)| {
                                            html!{
                                                <PipelineComponent project_id=tuple.0 pipeline_basic=tuple.1.clone() />
                                            }
                                        }).collect::<Html>()
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        }
    }
}

type JsonResponse<T> = Response<Json<Result<T, Error>>>;

impl Dashboard {
    pub fn add_project(&mut self, project: Project) {
        let project_id = project.id;
        let it_seed = (rand::random::<f64>() * 1000f64).round() as u64;
        let base_secs_it = GITLAB_SERVICE
            .read()
            .unwrap()
            .get_config()
            .update_project_details_interval_secs;
        let update_infos_callback = self
            .link
            .callback(move |_| Msg::UpdateProjectInfos(project_id));
        self.update_details_it.insert(
            project_id,
            IntervalService::spawn(
                Duration::from_millis(base_secs_it * 1000 + it_seed),
                update_infos_callback,
            ),
        );

        // MUTEX
        {
            let mut gitlab_service_w = GITLAB_SERVICE.write().unwrap_throw();
            gitlab_service_w.update_project(project);
        }

        self.update_project(project_id);
        self.update_pipelines(project_id);
        self.update_merge_request_count(project_id);
    }

    pub fn add_public_projects_callback(
        &self,
        page: usize,
    ) -> Callback<JsonResponse<Vec<Project>>> {
        callback!(
            self,
            move |projects: Vec<Project>| Some(Msg::AddPublicProjects { projects, page }),
            move |status: StatusCode| Some(Msg::Error(format!(
                "Cannot fetch public projects (status {})",
                status
            ))),
            move |e: Error| Some(Msg::Error(format!("Cannot add public projects ({:?})", e)))
        )
    }

    pub fn add_group_projects_callback(
        &self,
        group: &str,
        page: usize,
    ) -> Callback<JsonResponse<Vec<Project>>> {
        let group = group.to_string();
        let group_clone = group.clone();

        callback!(
            self,
            move |projects: Vec<Project>| Some(Msg::AddGroupProjects {
                namespace: group.clone(),
                projects,
                page,
            }),
            move |status: StatusCode| Some(Msg::Error(format!(
                "Cannot fetch project from group {} (status:{})",
                group_clone, status
            )))
        )
    }

    pub fn update_project(&mut self, project_id: ProjectId) {
        let callback = callback!(
            self,
            move |project: Project| {
                let mut gitlab_service_w = GITLAB_SERVICE.write().unwrap_throw();
                let default_branch = project.default_branch.clone();
                gitlab_service_w.update_project(project);
                Some(Msg::UpdateProjectDone(project_id, default_branch))
            },
            move |_status: StatusCode| None,
            move |_e: Error| None,
            move || Msg::UpdateDetailsDone(project_id)
        );
        let gitlab_service_r = GITLAB_SERVICE.read().unwrap_throw();
        self.projects_ft.insert(
            project_id,
            gitlab_service_r
                .fetch_project_from_id(project_id, callback)
                .unwrap_throw(),
        );
    }

    // Update detail
    pub fn update_details(&mut self, project_id: ProjectId, branch: Option<String>) {
        let callback = callback!(
            self,
            move |details: RepoCommitDetail| {
                let mut gitlab_service_w = GITLAB_SERVICE.write().unwrap_throw();
                gitlab_service_w.update_repo_details(project_id, details);
                None
            },
            move |_status: StatusCode| None,
            move |_e: Error| None,
            move || Msg::UpdateDetailsDone(project_id)
        );

        let gitlab_service_r = GITLAB_SERVICE.read().unwrap_throw();
        self.details_ft.insert(
            project_id,
            gitlab_service_r
                .fetch_project_details_from_branch(
                    project_id,
                    &branch.unwrap_or_else(|| "master".to_string()),
                    callback,
                )
                .unwrap_throw(),
        );
    }

    pub fn update_pipelines(&mut self, project_id: ProjectId) {
        let callback = callback!(
            self,
            move |pipelines: Vec<PipelineBasic>| Some(Msg::AddPipelines((project_id, pipelines))),
            move |status: StatusCode| Some(Msg::Error(format!(
                "Cannot get pipelines (status: {})",
                status
            )))
        );
        let gitlab_service_r = GITLAB_SERVICE.read().unwrap_throw();
        self.pipelines_ft.insert(
            project_id,
            gitlab_service_r
                .fetch_pipelines_from_project(project_id, callback)
                .unwrap_throw(),
        );
    }

    fn update_merge_request_count(&mut self, project_id: ProjectId) {
        debug!("Update MR counter");
        let callback = self
            .link
            .callback(move |response: JsonResponse<Vec<MergeRequest>>| {
                let (meta, Json(_)) = response.into_parts();
                if meta.status == StatusCode::OK {
                    match meta.headers.get("X-Total") {
                        Some(total) => {
                            let count: u64 = total.to_str().unwrap().parse().unwrap();
                            let mut gitlab_service_w = GITLAB_SERVICE.write().unwrap_throw();
                            gitlab_service_w.update_merge_request_count(project_id, count);
                        }
                        None => error!(
                            "Cannot update merge request counter details {} ",
                            project_id
                        ),
                    }
                } else {
                    warn!(
                        "Cannot commits to project (id={}) code : {}",
                        project_id, meta.status
                    );
                }

                Msg::UpdateMergeRequestCounterDone(project_id)
            });
        let gitlab_service_r = GITLAB_SERVICE.read().unwrap_throw();
        self.merge_request_count_ft.insert(
            project_id,
            gitlab_service_r
                .fetch_merge_request_count(project_id, callback)
                .unwrap_throw(),
        );
    }

    fn _macro_callback<T, F, G, H, I>(
        &self,
        ok_closure: F,
        nok_closure: G,
        err_closure: H,
        default_closure: I,
    ) -> Callback<JsonResponse<T>>
    where
        F: Fn(T) -> Option<Msg> + 'static,
        G: Fn(StatusCode) -> Option<Msg> + 'static,
        H: Fn(Error) -> Option<Msg> + 'static,
        I: Fn() -> Msg + 'static,
    {
        self.link
            .callback(move |response: Response<Json<Result<T, Error>>>| {
                let (meta, Json(res)) = response.into_parts();
                if meta.status == StatusCode::OK {
                    match res {
                        Ok(data) => {
                            let m_opt = ok_closure(data);
                            if let Some(m_opt_s) = m_opt {
                                return m_opt_s;
                            }
                        }
                        Err(e) => {
                            warn!("Error on {} due to {:?})", std::any::type_name::<T>(), e);
                            let m_opt = err_closure(e);
                            if let Some(m_opt_s) = m_opt {
                                return m_opt_s;
                            }
                        }
                    }
                } else {
                    warn!(
                        "Cannot fetch {} (status={})",
                        std::any::type_name::<T>(),
                        meta.status
                    );
                    let m_opt = nok_closure(meta.status);
                    if let Some(m_opt_s) = m_opt {
                        return m_opt_s;
                    }
                }
                default_closure()
            })
    }
}
