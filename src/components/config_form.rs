use crate::config::{AppConfig, AppConfigError};
use log::{debug, warn};
use url::Url;
use wasm_bindgen::{prelude::*, JsCast, UnwrapThrowExt};
use web_sys::{window, FormData, HtmlFormElement};
use yew::prelude::*;

const ID_FORM_URL: &str = "url";
const ID_FORM_TOKEN: &str = "token";
const ID_FORM_GROUPS: &str = "groups";
const ID_FORM_PROJECTS: &str = "projects";
const ID_FORM_CARD_PER_LINE: &str = "per_line";
const ID_FORM_PIPELINES_PER_PROJECT: &str = "pipelines_per_project";
const ID_FORM_PIPELINES_EXPIRATION_DAYS: &str = "pipelines_expiration_days";
const ID_FORM_PIPELINES_EXPIRATION_HOURS: &str = "pipelines_expiration_hours";
const ID_FORM_PIPELINES_EXPIRATION_MINUTES: &str = "pipelines_expiration_minutes";
const ID_FORM_ALL_PROJECT: &str = "all_projects";
const ID_FORM_UPDATE_PROJECT_DETAILS_INTERVAL: &str = "update_project_details_interval";
const ID_FORM_UPDATE_PROJECTS_LIST_INTERVAL: &str = "update_projects_list_interval";
const ID_FORM_UPDATE_JOBS_INTERVAL: &str = "update_jobs_interval";
const ID_FORM_WITHOUT_CI: &str = "without_ci";
const ID_FORM_DARK_MODE: &str = "dark_mode";

#[wasm_bindgen]
extern "C" {
    type TagsInput;

    #[wasm_bindgen(constructor)]
    fn init_input_tag(input_id: &str) -> TagsInput;

    #[wasm_bindgen(method)]
    fn getInputString(this: &TagsInput) -> String;

    #[wasm_bindgen(method)]
    fn addTag(this: &TagsInput, tag: String);

}

fn parse_url(form: &FormData, id: &str) -> Result<Url, AppConfigError> {
    Ok(Url::parse(
        &form.get(id).as_string().ok_or(AppConfigError::NoneValue)?,
    )?)
}

fn parse_string(form: &FormData, id: &str) -> Result<String, AppConfigError> {
    form.get(id).as_string().ok_or(AppConfigError::NoneValue)
}

fn parse_vec_string(tag_input_option: &Option<TagsInput>) -> Result<Vec<String>, AppConfigError> {
    match tag_input_option {
        Some(tag_input) => {
            debug!("Vec string {}", tag_input.getInputString());
            Ok(tag_input
                .getInputString()
                .split(',')
                .map(String::from)
                .filter(|v| !v.is_empty())
                .collect())
        }
        None => {
            debug!("No Vec string");
            Err(AppConfigError::NoneValue)
        }
    }
}

fn parse_u64(form: &FormData, id: &str) -> Result<u64, AppConfigError> {
    form.get(id)
        .as_string()
        .ok_or(AppConfigError::NoneValue)?
        .parse()
        .map_err(|_| AppConfigError::ParseNumber)
}

pub struct ConfigForm {
    link: ComponentLink<Self>,
    config: AppConfig,
    errors: Vec<(String, AppConfigError)>,
    collapse: bool,

    form_groups_tags: Option<TagsInput>,
    form_projects_tags: Option<TagsInput>,
}

pub enum ConfigFormMsg {
    Generate(FormData),
    Collapse,
    Reload(String),
}

#[derive(Properties, Clone)]
pub struct ConfigFormProps {
    pub config: Option<AppConfig>,
}

impl Component for ConfigForm {
    type Message = ConfigFormMsg;
    type Properties = ConfigFormProps;

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        true
    }

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        ConfigForm {
            link,
            config: props.config.unwrap_or_default(),
            errors: Vec::new(),
            collapse: true,
            form_groups_tags: None,
            form_projects_tags: None,
        }
    }

    fn rendered(&mut self, first_render: bool) {
        if first_render {
            let groups_input = TagsInput::init_input_tag(ID_FORM_GROUPS);
            for t in self.config.groups.iter() {
                if !t.trim().is_empty() {
                    groups_input.addTag(t.to_string());
                }
            }
            self.form_groups_tags = Some(groups_input);

            let projects_input = TagsInput::init_input_tag(ID_FORM_PROJECTS);
            for t in self.config.projects.iter() {
                if !t.trim().is_empty() {
                    projects_input.addTag(t.to_string());
                }
            }
            self.form_projects_tags = Some(projects_input);
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Self::Message::Generate(form) => {
                debug!("Generate");
                self.errors = Vec::new();
                self.collapse = false;
                self.config.pipelines_expiration_secs = 0;

                match parse_url(&form, ID_FORM_URL) {
                    Ok(data) => self.config.api.url = data,
                    Err(err) => self.errors.push((ID_FORM_URL.to_string(), err)),
                }
                match parse_string(&form, ID_FORM_TOKEN) {
                    Ok(data) => self.config.api.token = data,
                    Err(err) => self.errors.push((ID_FORM_TOKEN.to_string(), err)),
                }
                match parse_vec_string(&self.form_groups_tags) {
                    Ok(data) => self.config.groups = data,
                    Err(err) => self.errors.push((ID_FORM_GROUPS.to_string(), err)),
                }
                match parse_vec_string(&self.form_projects_tags) {
                    Ok(data) => self.config.projects = data,
                    Err(err) => self.errors.push((ID_FORM_PROJECTS.to_string(), err)),
                }
                match parse_u64(&form, ID_FORM_CARD_PER_LINE) {
                    Ok(data) => self.config.per_line = data,
                    Err(err) => self.errors.push((ID_FORM_CARD_PER_LINE.to_string(), err)),
                }
                match parse_u64(&form, ID_FORM_UPDATE_PROJECTS_LIST_INTERVAL) {
                    Ok(data) => self.config.update_projects_list_interval_secs = data,
                    Err(err) => self
                        .errors
                        .push((ID_FORM_UPDATE_PROJECTS_LIST_INTERVAL.to_string(), err)),
                }
                match parse_u64(&form, ID_FORM_UPDATE_PROJECT_DETAILS_INTERVAL) {
                    Ok(data) => self.config.update_project_details_interval_secs = data,
                    Err(err) => self
                        .errors
                        .push((ID_FORM_UPDATE_PROJECT_DETAILS_INTERVAL.to_string(), err)),
                }
                match parse_u64(&form, ID_FORM_UPDATE_JOBS_INTERVAL) {
                    Ok(data) => self.config.update_jobs_interval_secs = data,
                    Err(err) => self
                        .errors
                        .push((ID_FORM_UPDATE_JOBS_INTERVAL.to_string(), err)),
                }
                match parse_u64(&form, ID_FORM_PIPELINES_PER_PROJECT) {
                    Ok(data) => self.config.pipelines_per_project = data,
                    Err(err) => self
                        .errors
                        .push((ID_FORM_PIPELINES_PER_PROJECT.to_string(), err)),
                }
                match parse_u64(&form, ID_FORM_PIPELINES_EXPIRATION_DAYS) {
                    Ok(data) => self.config.pipelines_expiration_secs += data * 3600 * 24,
                    Err(err) => self
                        .errors
                        .push((ID_FORM_PIPELINES_EXPIRATION_DAYS.to_string(), err)),
                }
                match parse_u64(&form, ID_FORM_PIPELINES_EXPIRATION_HOURS) {
                    Ok(data) => self.config.pipelines_expiration_secs += data * 3600,
                    Err(err) => self
                        .errors
                        .push((ID_FORM_PIPELINES_EXPIRATION_HOURS.to_string(), err)),
                }
                match parse_u64(&form, ID_FORM_PIPELINES_EXPIRATION_MINUTES) {
                    Ok(data) => self.config.pipelines_expiration_secs += data * 60,
                    Err(err) => self
                        .errors
                        .push((ID_FORM_PIPELINES_EXPIRATION_MINUTES.to_string(), err)),
                }
                self.config.public_projects = form.has(ID_FORM_ALL_PROJECT);
                self.config.hide_no_ci = form.has(ID_FORM_WITHOUT_CI);
                self.config.dark_mode = form.has(ID_FORM_DARK_MODE);

                debug!("Config: {:?}", self.config);
                if !self.errors.is_empty() {
                    warn!("Errors: {:?}", self.errors);
                }
                true
            }
            Self::Message::Collapse => {
                if self.collapse {
                    false
                } else {
                    self.collapse = true;
                    true
                }
            }
            Self::Message::Reload(link) => {
                window().unwrap().location().set_href(&link).unwrap();
                window().unwrap().location().reload().unwrap();
                true
            }
        }
    }

    fn view(&self) -> Html {
        let base = self.config.pipelines_expiration_secs;
        let mins = (base / 60) % 60;
        let hours = ((base - mins * 60) / 3600) % 24;
        let days = (base - mins * 60 - hours * 3600) / (24 * 3600);
        debug!("{}", mins);
        let onevent_collapse = &self.link.callback(|_| Self::Message::Collapse);
        html! {
            <div class="modal-dialog modal-dialog-scrollable modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{"Settings"}</h5>
                        {
                            if !&self.config.api.token.is_empty() {
                                html!{
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">{"×"}</span>
                                    </button>
                                }
                            } else {
                                html!{}
                            }
                        }
                    </div>
                    <div class="modal-body">
                        <form id="settingsForm" onsubmit=self.link.callback(|e: FocusEvent| {
                            e.prevent_default();
                            let form_element: HtmlFormElement = e.target()
                            .unwrap_throw()
                            .dyn_into::<HtmlFormElement>()
                            .unwrap_throw();
                            let form_data = FormData::new_with_form(&form_element).unwrap_throw();
                            Self::Message::Generate(form_data)
                        })>
                            <div class="form-group">
                                <label for=ID_FORM_URL>{"API URL (with trailing slash)"}</label>
                                <input class="form-control" id=ID_FORM_URL name=ID_FORM_URL placeholder="Base url" value=self.config.api.url.to_string() oninput=onevent_collapse required=true />
                            </div>
                            <div class="form-group">
                                <label for=ID_FORM_TOKEN>{"API private token"}</label>
                                <input class="form-control" id=ID_FORM_TOKEN name=ID_FORM_TOKEN placeholder="Token" value=self.config.api.token.clone() oninput=onevent_collapse required=true />
                            </div>
                            <div class="form-group">
                                <label for=ID_FORM_GROUPS>{"Groups (coma separated)"}</label>
                                <input class="form-control" id=ID_FORM_GROUPS name=ID_FORM_GROUPS placeholder="Groups" oninput=onevent_collapse />
                            </div>
                            <div class="form-group">
                                <label for=ID_FORM_PROJECTS>{"Projects (coma separated)"}</label>
                                <input class="form-control" id=ID_FORM_PROJECTS name=ID_FORM_PROJECTS placeholder="Projects" oninput=onevent_collapse />
                            </div>
                            <div class="form-group">
                                <label for=ID_FORM_CARD_PER_LINE>{"Card per line"}</label>
                                <input class="form-control" id=ID_FORM_CARD_PER_LINE name=ID_FORM_CARD_PER_LINE placeholder="Card per line" value=self.config.per_line.to_string() oninput=onevent_collapse required=true type="number" step="1"/>
                            </div>
                            <div class="form-group">
                                <label for=ID_FORM_UPDATE_PROJECTS_LIST_INTERVAL>{"Update projects list interval (secs)"}</label>
                                <input class="form-control" id=ID_FORM_UPDATE_PROJECTS_LIST_INTERVAL name=ID_FORM_UPDATE_PROJECTS_LIST_INTERVAL placeholder="Seconds" value=self.config.update_projects_list_interval_secs.to_string() oninput=onevent_collapse required=true type="number" min="1" step="1"/>
                            </div>
                            <div class="form-group">
                                <label for=ID_FORM_UPDATE_PROJECT_DETAILS_INTERVAL>{"Update project details interval (secs)"}</label>
                                <input class="form-control" id=ID_FORM_UPDATE_PROJECT_DETAILS_INTERVAL name=ID_FORM_UPDATE_PROJECT_DETAILS_INTERVAL placeholder="Seconds" value=self.config.update_project_details_interval_secs.to_string() oninput=onevent_collapse required=true type="number" min="1" step="1"/>
                            </div>
                            <div class="form-group">
                                <label for=ID_FORM_UPDATE_JOBS_INTERVAL>{"Update jobs details interval (secs)"}</label>
                                <input class="form-control" id=ID_FORM_UPDATE_JOBS_INTERVAL name=ID_FORM_UPDATE_JOBS_INTERVAL placeholder="Seconds" value=self.config.update_jobs_interval_secs.to_string() oninput=onevent_collapse required=true type="number" min="1" step="1"/>
                            </div>
                            <div class="form-group">
                                <label for=ID_FORM_PIPELINES_PER_PROJECT>{"Pipelines per project"}</label>
                                <input class="form-control" id=ID_FORM_PIPELINES_PER_PROJECT name=ID_FORM_PIPELINES_PER_PROJECT placeholder="Number" value=self.config.pipelines_per_project.to_string() oninput=onevent_collapse required=true type="number" min="1" max="100" step="1"/>
                            </div>
                            <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label class="form-group" for=ID_FORM_PIPELINES_EXPIRATION_DAYS>{"Pipelines updated after (days)"}</label>
                                        <input class="form-control" id=ID_FORM_PIPELINES_EXPIRATION_DAYS name=ID_FORM_PIPELINES_EXPIRATION_DAYS placeholder="Days" value=days.to_string() oninput=onevent_collapse required=true autocomplete="off" type="number" min="0" step="1"/>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-group" for=ID_FORM_PIPELINES_EXPIRATION_HOURS>{"Pipelines updated after (hours)"}</label>
                                        <input class="form-control" id=ID_FORM_PIPELINES_EXPIRATION_HOURS name=ID_FORM_PIPELINES_EXPIRATION_HOURS placeholder="Hours" value=hours.to_string() oninput=onevent_collapse required=true autocomplete="off" type="number" min="0" max="23" step="1"/>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-group" for=ID_FORM_PIPELINES_EXPIRATION_MINUTES>{"Pipelines updated after (minutes)"}</label>
                                        <input class="form-control" id=ID_FORM_PIPELINES_EXPIRATION_MINUTES name=ID_FORM_PIPELINES_EXPIRATION_MINUTES placeholder="Minutes" value=mins.to_string() oninput=onevent_collapse required=true autocomplete="off" type="number" min="0" max="59" step="1"/>
                                    </div>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" id=ID_FORM_ALL_PROJECT name=ID_FORM_ALL_PROJECT type="checkbox" checked=self.config.public_projects/>
                                <label class="form-check-label" for=ID_FORM_ALL_PROJECT>{"Show all visible projects"}</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" id=ID_FORM_WITHOUT_CI name=ID_FORM_WITHOUT_CI type="checkbox" checked=self.config.hide_no_ci/>
                                <label class="form-check-label" for=ID_FORM_WITHOUT_CI>{"Hide project without CI"}</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" id=ID_FORM_DARK_MODE name=ID_FORM_DARK_MODE type="checkbox" checked=self.config.dark_mode/>
                                <label class="form-check-label" for=ID_FORM_DARK_MODE>{"Dark mode"}</label>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="submit" class="btn btn-primary" form="settingsForm">{"Generate"}</button>
                        {
                            if !self.collapse{
                                if self.errors.is_empty() {
                                    let location = window().unwrap().location();
                                    let link = format!("{}{}#config={}", location.origin().unwrap(), location.pathname().unwrap(), self.config );
                                    let link_clone = link.clone();
                                    html! {
                                        <div class="alert alert-secondary break-text" role="alert">
                                            <p>
                                                <b>{"Use this url"}</b>
                                                <button onclick=self.link.callback(move |_| Self::Message::Reload(link_clone.clone())) class="btn btn-primary float-right text-white">{"Apply"}</button>
                                            </p>
                                            <p>{link.clone()}</p>
                                        </div>
                                    }
                                } else {
                                    html! {
                                        <div class="alert alert-danger" role="alert">
                                        {
                                            self.errors.iter().map(|error| {
                                                html!{
                                                    <div>
                                                        <b>{&error.0}</b>{": "} {&error.1}
                                                    </div>
                                                }
                                            }).collect::<Html>()
                                        }
                                        </div>
                                    }
                                }
                            } else {
                                html! {}
                            }
                        }
                    </div>
                </div>
            </div>
        }
    }
}
