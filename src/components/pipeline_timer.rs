use chrono::{DateTime, Utc};
use gitlab::StatusState;
use humantime::format_duration;
use wasm_bindgen::UnwrapThrowExt;
use yew::{prelude::*, services::interval::IntervalTask, services::IntervalService};

pub struct PipelineTimerComponent {
    pipeline_status: StatusState,
    start_date: DateTime<Utc>,
    end_date: Option<DateTime<Utc>>,
    timeago: bool,
    interval: u64,
    _update_timer: IntervalTask,
    link: ComponentLink<Self>,
}

#[derive(Properties, Clone)]
pub struct PipelineTimerProps {
    pub pipeline_status: StatusState,
    pub start_date: DateTime<Utc>,
    pub end_date: Option<DateTime<Utc>>,
    pub timeago: bool,
    pub interval: u64,
}

pub enum Msg {
    UpdatePipelineTimer,
}

impl Component for PipelineTimerComponent {
    type Message = Msg;
    type Properties = PipelineTimerProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        // Interval Task updating pipeline timer
        let update_timer_callback = link.callback(|_| Msg::UpdatePipelineTimer);
        let update_timer = IntervalService::spawn(
            std::time::Duration::from_secs(props.interval),
            update_timer_callback,
        );

        let mut timer = PipelineTimerComponent {
            pipeline_status: props.pipeline_status,
            start_date: props.start_date,
            end_date: props.end_date,
            timeago: props.timeago,
            interval: props.interval,
            _update_timer: update_timer,
            link,
        };
        timer.update(Msg::UpdatePipelineTimer);
        timer
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::UpdatePipelineTimer => self.end_date.is_none(),
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if props.pipeline_status != self.pipeline_status
            || self.start_date != props.start_date
            || self.timeago != props.timeago
            || self.interval != props.interval
        {
            self.pipeline_status = props.pipeline_status;
            self.start_date = props.start_date;
            self.end_date = props.end_date;
            self.timeago = props.timeago;
            if self.interval != props.interval {
                self.interval = props.interval;
                let update_timer_callback = self.link.callback(|_| Msg::UpdatePipelineTimer);
                let update_timer = IntervalService::spawn(
                    std::time::Duration::from_secs(props.interval),
                    update_timer_callback,
                );
                self._update_timer = update_timer;
            }
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let real_end_date =
            if self.pipeline_status == StatusState::Running || self.end_date.is_none() {
                Utc::now()
            } else {
                self.end_date.unwrap_throw()
            };

        if self.timeago {
            let formatter = timeago::Formatter::new();
            html! {
                {format!(" {}", formatter.convert((real_end_date - self.start_date).to_std().unwrap_throw()))}
            }
        } else {
            let duration: chrono::Duration = real_end_date - self.start_date;
            let duration_seconds_precision =
                std::time::Duration::from_secs(duration.to_std().unwrap_throw().as_secs());
            html! {
                {format!(" {}", format_duration(duration_seconds_precision))}
            }
        }
    }
}
