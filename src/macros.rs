#[macro_export]
macro_rules! callback {
    ($self:ident, $ok_closure: expr) => {
        $self._macro_callback(
            $ok_closure,
            move |status: StatusCode| Some(Msg::Error(format!("Cannot fetch (status:{})", status))),
            move |e: Error| None,
            move || unimplemented!(""),
        )
    };
    ($self:ident, $ok_closure: expr, $nok_closure: expr) => {
        $self._macro_callback(
            $ok_closure,
            $nok_closure,
            move |_e: Error| None,
            move || unimplemented!(""),
        )
    };
    ($self:ident, $ok_closure: expr, $nok_closure: expr, $err_closure: expr) => {
        $self._macro_callback($ok_closure, $nok_closure, $err_closure, move || {
            unimplemented!("")
        })
    };
    ($self:ident, $ok_closure: expr, $nok_closure: expr, $err_closure: expr, $default_closure: expr) => {
        $self._macro_callback($ok_closure, $nok_closure, $err_closure, $default_closure)
    };
}
