use err_derive::Error;
use serde::{Deserialize, Serialize};
use smart_default::SmartDefault;
use std::{
    fmt::{Debug, Display},
    str::FromStr,
};
use url::Url;
use wasm_bindgen::UnwrapThrowExt;

#[derive(Debug, Error)]
pub enum AppConfigError {
    #[error(display = "Could not parse url: {}", _0)]
    UrlParse(#[error(source)] url::ParseError),
    #[error(display = "Could not parse value")]
    ParseNumber,
    #[error(display = "Value is not present")]
    NoneValue,
    #[error(display = "Base62 error")]
    Base62Error,
    #[error(display = "SerdeJson error: {}", _0)]
    SerdeError(#[error(source)] serde_json::Error),
}

#[derive(Serialize, Deserialize, Debug, Clone, SmartDefault)]
pub struct APIConfig {
    #[default(_code = "Url::parse(\"https://framagit.org/api/v4/\").unwrap_throw()")]
    pub url: Url,
    pub token: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, SmartDefault)]
pub struct AppConfig {
    pub api: APIConfig,
    pub groups: Vec<String>,
    pub projects: Vec<String>,
    #[default = 0]
    pub per_line: u64,
    #[default = 5]
    pub pipelines_per_project: u64,
    #[default = 3600]
    pub pipelines_expiration_secs: u64,
    pub public_projects: bool,
    pub hide_no_ci: bool,
    #[serde(default = "default_as_false")]
    pub dark_mode: bool,
    #[default = 120]
    pub update_projects_list_interval_secs: u64,
    #[default = 30]
    pub update_project_details_interval_secs: u64,
    #[default = 30]
    pub update_jobs_interval_secs: u64,
}

impl FromStr for AppConfig {
    type Err = AppConfigError;
    fn from_str(string: &str) -> std::result::Result<Self, <Self as std::str::FromStr>::Err> {
        let json = base_62::decode(string).map_err(|_| AppConfigError::Base62Error)?;
        let form: AppConfig = serde_json::from_slice(&json).map_err(AppConfigError::SerdeError)?;
        Ok(form)
    }
}

impl AppConfig {
    fn to_base_62(&self) -> String {
        base_62::encode(serde_json::to_string(&self).unwrap_throw().as_bytes())
    }
}

impl Display for AppConfig {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.to_base_62())
    }
}

fn default_as_false() -> bool {
    false
}
