#![recursion_limit = "4096"]

pub mod components;
pub mod config;
pub mod gitlab_service;
pub mod macros;

use crate::{components::app, gitlab_service::GitlabService};
use lazy_static::lazy_static;
use std::sync::RwLock;
use wasm_bindgen::prelude::*;

lazy_static! {
    pub static ref GITLAB_SERVICE: RwLock<GitlabService> = RwLock::new(GitlabService::default());
}
#[wasm_bindgen]
pub fn init_panic_hook() {
    console_error_panic_hook::set_once();
}
#[wasm_bindgen]
pub fn run_app() -> Result<(), JsValue> {
    init_panic_hook();
    wasm_logger::init(wasm_logger::Config::default());
    yew::start_app::<app::App>();

    Ok(())
}
