use crate::config::AppConfig;
use anyhow::Error;
use chrono::{prelude::*, Duration};
use gitlab::{Job, Pipeline, PipelineBasic, PipelineId, Project, ProjectId, RepoCommitDetail};
use log::{debug, trace};
use std::collections::{BTreeMap, HashMap};
use unicase::UniCase;
use url::form_urlencoded::byte_serialize;
use wasm_bindgen::prelude::*;
use yew::{
    format::{Nothing, Text},
    services::{
        fetch::{FetchTask, Request, Response},
        FetchService,
    },
    Callback,
};

pub const PER_PAGE: usize = 100;

#[derive(Clone)]
pub enum LastPipeline {
    Unknown,
    Pipeline(Box<Pipeline>),
    NotAvailable,
    NetworkError,
}

/// Helper to convert gitlab project.name_with_namespace to a printable Namespace
fn name_with_namespace_to_string(project: &Project) -> UniCase<String> {
    // Crop to remove project name
    let namespace = match project.name_with_namespace.rfind('/') {
        Some(offset) => project.name_with_namespace.split_at(offset).0.to_string(),
        None => project.name_with_namespace.clone(),
    };
    UniCase::new(namespace)
}

/// Gitlab Service store every data from data API.
/// All field must be keept private to avoid dirty update.
pub struct GitlabService {
    /// List all watched project
    projects: HashMap<ProjectId, Project>,
    /// Project grouped by Namespace.
    /// BTreeMap<namespace_path, BTreeMap<project_name, ProjectId>>
    projects_namespaces: BTreeMap<UniCase<String>, BTreeMap<UniCase<String>, ProjectId>>,
    /// Open merge request count
    projects_merge_request_count: HashMap<ProjectId, u64>,
    /// Pipelines sorted by PipelineId.
    /// All pipeline are linked to its own ProjectId because it's needed for API calls and not present in Pipeline type.
    pipelines: BTreeMap<PipelineId, (ProjectId, PipelineBasic)>,
    /// Jobs grouped by related Pipeline.
    jobs: BTreeMap<PipelineId, Vec<Job>>,
    /// Repo details by ProjectId
    repo_commit_status: HashMap<ProjectId, RepoCommitDetail>,
    /// LastPipeline state by ProjectId
    last_pipelines: HashMap<ProjectId, LastPipeline>,
    /// App config
    config: AppConfig,
}

impl Default for GitlabService {
    fn default() -> Self {
        Self {
            projects: HashMap::new(),
            projects_namespaces: BTreeMap::new(),
            projects_merge_request_count: HashMap::new(),
            pipelines: BTreeMap::new(),
            jobs: BTreeMap::new(),
            repo_commit_status: HashMap::new(),
            last_pipelines: HashMap::new(),
            config: AppConfig::default(),
        }
    }
}
impl GitlabService {
    pub fn get_last_pipeline(&self, project_id: &ProjectId) -> LastPipeline {
        let lat = self
            .last_pipelines
            .get(project_id)
            .unwrap_or(&LastPipeline::Unknown);
        lat.clone()
    }
    pub fn get_projects_group_by_group(
        &self,
    ) -> &BTreeMap<UniCase<String>, BTreeMap<UniCase<String>, ProjectId>> {
        &self.projects_namespaces
    }
    pub fn get_pipelines(&self) -> &BTreeMap<PipelineId, (ProjectId, PipelineBasic)> {
        &self.pipelines
    }
    pub fn get_last_pipelines(&self) -> &HashMap<ProjectId, LastPipeline> {
        &self.last_pipelines
    }
    pub fn get_repo_commit_status(&self) -> &HashMap<ProjectId, RepoCommitDetail> {
        &self.repo_commit_status
    }

    pub fn get_merge_request_count(&self, project_id: &ProjectId) -> Option<&u64> {
        self.projects_merge_request_count.get(project_id)
    }

    pub fn update_merge_request_count(
        &mut self,
        project_id: ProjectId,
        mr_counter: u64,
    ) -> Option<u64> {
        self.projects_merge_request_count
            .insert(project_id, mr_counter)
    }

    /// Helper to clean all data of a project in projects_namespaces struct
    fn clean_project_struct(&mut self, project_id: &ProjectId) -> Option<Project> {
        match self.projects.remove(project_id) {
            Some(project) => {
                let namespace = name_with_namespace_to_string(&project);

                if let Some(projects) = self.projects_namespaces.get_mut(&namespace) {
                    let keys: Vec<UniCase<String>> = projects
                        .iter()
                        .filter(|&(_, v)| v == &project.id)
                        .map(|(k, _)| k.clone())
                        .collect();
                    keys.into_iter().map(|k| projects.remove(&k)).for_each(drop);
                    if projects.is_empty() {
                        debug!("drop namespace {}", &namespace);
                        self.projects_namespaces.remove(&namespace);
                    }
                }
                Some(project)
            }
            None => None,
        }
    }

    /// Update project in GitlabService.
    pub fn update_project(&mut self, project: Project) {
        trace!("[GITLAB_SERVICE] Update project {}", &project.id);
        // Looking for and update the related namespace relationship
        let namespace_fullname = name_with_namespace_to_string(&project);

        let insert_namespace = match self.projects.insert(project.id, project.clone()) {
            None => true,
            Some(old_project) => {
                let namespace_fullname_old = name_with_namespace_to_string(&old_project);
                if namespace_fullname != namespace_fullname_old {
                    trace!("[GITLAB_SERVICE] Project has been moved");
                    self.clean_project_struct(&old_project.id);
                    true
                } else {
                    false
                }
            }
        };

        if insert_namespace {
            let projects = match self.projects_namespaces.get_mut(&namespace_fullname) {
                Some(projects) => projects,
                None => {
                    self.projects_namespaces
                        .insert(namespace_fullname.clone(), BTreeMap::new());
                    self.projects_namespaces
                        .get_mut(&namespace_fullname)
                        .unwrap_throw()
                }
            };
            projects.insert(UniCase::new(project.name), project.id);
        }
    }

    pub fn update_pipeline(
        &mut self,
        project_id: ProjectId,
        pipeline: PipelineBasic,
    ) -> Option<(ProjectId, PipelineBasic)> {
        self.pipelines.insert(pipeline.id, (project_id, pipeline))
    }

    pub fn remove_old_pipelines(&mut self) {
        let removing_pipeline_ids: Vec<PipelineId> = self
            .pipelines
            .iter()
            .filter_map(|(k, v)| {
                if let Some(updated_at) = v.1.updated_at {
                    let max_fetch_date = Utc::now()
                        - Duration::seconds(self.config.pipelines_expiration_secs as i64);
                    if updated_at < max_fetch_date {
                        Some(*k)
                    } else {
                        None
                    }
                } else {
                    None
                }
            })
            .collect();
        removing_pipeline_ids
            .into_iter()
            .map(|pipeline_id| self.remove_pipeline(&pipeline_id))
            .for_each(drop);
    }

    fn remove_pipeline(&mut self, pipeline_id: &PipelineId) -> Option<(ProjectId, PipelineBasic)> {
        self.jobs.remove(pipeline_id);
        self.pipelines.remove(pipeline_id)
    }

    fn remove_project(&mut self, project_id: ProjectId) -> Option<Project> {
        debug!("drop project {}", project_id);

        self.repo_commit_status.remove(&project_id);
        self.last_pipelines.remove(&project_id);

        let removing_pipeline_ids: Vec<PipelineId> = self
            .pipelines
            .iter()
            .filter_map(|(k, v)| if v.0 == project_id { Some(*k) } else { None })
            .collect();

        removing_pipeline_ids
            .into_iter()
            .map(|pipeline_id| self.remove_pipeline(&pipeline_id))
            .for_each(drop);

        self.clean_project_struct(&project_id)
    }

    pub fn update_last_pipeline(
        &mut self,
        project_id: ProjectId,
        last_pipeline: LastPipeline,
    ) -> Option<LastPipeline> {
        if self.config.hide_no_ci {
            match self.get_last_pipeline(&project_id) {
                LastPipeline::Pipeline(previous_last_pipeline) => match last_pipeline {
                    LastPipeline::NetworkError => {
                        Some(LastPipeline::Pipeline(previous_last_pipeline))
                    }
                    LastPipeline::NotAvailable => {
                        self.remove_project(project_id);
                        self.last_pipelines.remove(&project_id)
                    }
                    _ => self.last_pipelines.insert(project_id, last_pipeline),
                },
                _ => match last_pipeline {
                    LastPipeline::NotAvailable => {
                        self.remove_project(project_id);
                        self.last_pipelines.remove(&project_id)
                    }
                    _ => self.last_pipelines.insert(project_id, last_pipeline),
                },
            }
        } else {
            match self.get_last_pipeline(&project_id) {
                LastPipeline::Pipeline(previous_last_pipeline) => match last_pipeline {
                    LastPipeline::NetworkError => {
                        Some(LastPipeline::Pipeline(previous_last_pipeline))
                    }
                    _ => self.last_pipelines.insert(project_id, last_pipeline),
                },
                _ => self.last_pipelines.insert(project_id, last_pipeline),
            }
        }
    }
    pub fn update_repo_details(
        &mut self,
        project_id: ProjectId,
        repo_details: RepoCommitDetail,
    ) -> Option<RepoCommitDetail> {
        self.repo_commit_status.insert(project_id, repo_details)
    }

    pub fn fetch_project_from_id<OUT: 'static>(
        &self,
        project_id: ProjectId,
        callback: Callback<Response<OUT>>,
    ) -> Result<FetchTask, Error>
    where
        OUT: From<Text>,
    {
        self.fetch_project(&format!("{}", project_id), callback)
    }

    pub fn fetch_project_details_from_branch<OUT: 'static>(
        &self,
        project_id: ProjectId,
        branch: &str,
        callback: Callback<Response<OUT>>,
    ) -> Result<FetchTask, Error>
    where
        OUT: From<Text>,
    {
        let request = Request::get(
            self.config
                .api
                .url
                .join(&format!(
                    "projects/{}/repository/commits/{}",
                    project_id, branch
                ))
                .unwrap_throw()
                .as_str(),
        )
        .header("Private-Token", &self.config.api.token)
        .body(Nothing)
        .unwrap_throw();
        FetchService::fetch(request, callback)
    }

    pub fn fetch_merge_request_count<OUT: 'static>(
        &self,
        project_id: ProjectId,
        callback: Callback<Response<OUT>>,
    ) -> Result<FetchTask, Error>
    where
        OUT: From<Text>,
    {
        let request = Request::get(
            self.config
                .api
                .url
                .join(&format!(
                    "projects/{}/merge_requests?state=opened&per_page=1",
                    project_id
                ))
                .unwrap_throw()
                .as_str(),
        )
        .header("Private-Token", &self.config.api.token)
        .body(Nothing)
        .unwrap_throw();
        FetchService::fetch(request, callback)
    }

    pub fn get_project(&self, project_id: &ProjectId) -> Option<&Project> {
        self.projects.get(project_id)
    }

    pub fn update_config(&mut self, config: AppConfig) {
        self.config = config
    }

    pub fn get_config(&self) -> &AppConfig {
        &self.config
    }

    pub fn get_repo_details(&self, project_id: &ProjectId) -> Option<&RepoCommitDetail> {
        self.repo_commit_status.get(project_id)
    }

    pub fn fetch_pipeline_from_pipeline_id<OUT: 'static>(
        &self,
        project_id: ProjectId,
        pipeline_id: &PipelineId,
        callback: Callback<Response<OUT>>,
    ) -> Result<FetchTask, Error>
    where
        OUT: From<Text>,
    {
        let request = Request::get(
            self.config
                .api
                .url
                .join(&format!(
                    "projects/{}/pipelines/{}",
                    project_id, pipeline_id,
                ))
                .unwrap_throw()
                .as_str(),
        )
        .header("Private-Token", &self.config.api.token)
        .body(Nothing)
        .unwrap_throw();
        FetchService::fetch(request, callback)
    }

    pub fn fetch_pipelines_from_project<OUT: 'static>(
        &self,
        project_id: ProjectId,
        callback: Callback<Response<OUT>>,
    ) -> Result<FetchTask, Error>
    where
        OUT: From<Text>,
    {
        let formatted_date = (Utc::now()
            - Duration::seconds(self.config.pipelines_expiration_secs as i64))
        .format("%Y-%m-%dT%H:%M:%SZ")
        .to_string();
        let request = Request::get(
            self.config
                .api
                .url
                .join(&format!(
                    "projects/{}/pipelines?per_page={}&page=1&updated_after={}",
                    project_id, self.config.pipelines_per_project, formatted_date
                ))
                .unwrap_throw()
                .as_str(),
        )
        .header("Private-Token", &self.config.api.token)
        .body(Nothing)
        .unwrap_throw();
        FetchService::fetch(request, callback)
    }

    pub fn fetch_projects_by_group<OUT: 'static>(
        &self,
        group_alias: &str,
        callback: Callback<Response<OUT>>,
        page: usize,
    ) -> Result<FetchTask, Error>
    where
        OUT: From<Text>,
    {
        let group = byte_serialize(group_alias.as_bytes()).collect::<String>();
        let request = Request::get(
            self.config
                .api
                .url
                .join(&format!(
                    "groups/{}/projects?per_page={}&page={}&include_subgroups=true&archived=false",
                    group, PER_PAGE, page,
                ))
                .unwrap_throw()
                .as_str(),
        )
        .header("Private-Token", &self.config.api.token)
        .body(Nothing)
        .unwrap_throw();
        FetchService::fetch(request, callback)
    }

    pub fn fetch_project_from_alias<OUT: 'static>(
        &self,
        project_alias: &str,
        callback: Callback<Response<OUT>>,
    ) -> Result<FetchTask, Error>
    where
        OUT: From<Text>,
    {
        let project = byte_serialize(project_alias.as_bytes()).collect::<String>();
        self.fetch_project(&project, callback)
    }

    pub fn fetch_public_projects<OUT: 'static>(
        &self,
        callback: Callback<Response<OUT>>,
        page: usize,
    ) -> Result<FetchTask, Error>
    where
        OUT: From<Text>,
    {
        let request = Request::get(
            self.config
                .api
                .url
                .join(&format!(
                    "projects/?per_page={}&page={}&archived=false",
                    PER_PAGE, page,
                ))
                .unwrap_throw()
                .as_str(),
        )
        .header("Private-Token", &self.config.api.token)
        .body(Nothing)
        .unwrap_throw();
        FetchService::fetch(request, callback)
    }

    pub fn get_all_project_ids(&self) -> Vec<ProjectId> {
        self.projects.keys().cloned().collect()
    }

    pub fn fetch_jobs_from_pipeline_id<OUT: 'static>(
        &self,
        project_id: ProjectId,
        pipeline_id: &PipelineId,
        callback: Callback<Response<OUT>>,
    ) -> Result<FetchTask, Error>
    where
        OUT: From<Text>,
    {
        let request = Request::get(
            self.config
                .api
                .url
                .join(&format!(
                    "projects/{}/pipelines/{}/jobs",
                    project_id, pipeline_id,
                ))
                .unwrap_throw()
                .as_str(),
        )
        .header("Private-Token", &self.config.api.token)
        .body(Nothing)
        .unwrap_throw();
        FetchService::fetch(request, callback)
    }

    pub fn get_pipeline(&self, pipeline_id: &PipelineId) -> Option<&(ProjectId, PipelineBasic)> {
        self.pipelines.get(pipeline_id)
    }

    pub fn update_jobs(
        &mut self,
        pipeline_id: &PipelineId,
        mut jobs: Vec<Job>,
    ) -> Option<Vec<Job>> {
        jobs.sort_by_key(|job| job.id);
        self.jobs.insert(*pipeline_id, jobs)
    }
    pub fn get_jobs(&self, pipeline_id: &PipelineId) -> Option<&Vec<Job>> {
        self.jobs.get(pipeline_id)
    }

    fn fetch_project<OUT: 'static>(
        &self,
        project: &str,
        callback: Callback<Response<OUT>>,
    ) -> Result<FetchTask, Error>
    where
        OUT: From<Text>,
    {
        let request = Request::get(
            self.config
                .api
                .url
                .join(&format!("projects/{}", project))
                .unwrap_throw()
                .as_str(),
        )
        .header("Private-Token", &self.config.api.token)
        .body(Nothing)
        .unwrap_throw();
        FetchService::fetch(request, callback)
    }
}
